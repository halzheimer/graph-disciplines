# Graph des disciplines des [notices Alzheimer dans HAL](https://hal.science/search/index?q=alzheimer)

Parcourir le graph interactif : [https://graph-disciplines-halzheimer-9f592dd501ed8a466a371216b6583c98d2.gitpages.huma-num.fr](https://graph-disciplines-halzheimer-9f592dd501ed8a466a371216b6583c98d2.gitpages.huma-num.fr)

![Illustration du graph interactif des domaines Alzheimer dans HAL](ban.png "Illustration du graph interactif des domaines Alzheimer dans HAL")